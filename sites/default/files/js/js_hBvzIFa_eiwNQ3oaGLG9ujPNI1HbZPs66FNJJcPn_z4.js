(function ($) {
  $(document).ready(function(){
  	var blockId = "all";
  	$(".view-initiatives-filter a").click(function(){
  		var id = $(this).attr("id");
  		switch(id) {
        // OEF Operations
        case "term-49":
          blockId = "views-people-block_8";
          break;
        // OEF Research
        case "term-52":
          blockId = "views-people-block_9";
          break;
  			// OBP
  			case "term-47":
  				blockId = "views-people-block_7";
  				break;
        // Secure Fisheries
        case "term-51":
          blockId = "views-people-block_6";
          break;
        // Shuraako
        case "term-48":
          blockId = "views-people-block_3";
          break;
        // Paso Colombia
        case "term-50":
          blockId = "views-people-block_2";
          break;
        // Our Secure Future
        case "term-120":
          blockId = "views-people-block_10";
          break;
        // Shared Resources
        case "term-146":
          blockId = "views-people-block_16";
          break;
  			default:
  				blockId = "all";
  				break;
  		}
  		if(blockId != "all") {
  			console.log(blockId);
  			$("#Content .col-md-12").each(function(){
  				if($(this).attr("id") != 'views-initiatives_filter-block') {
  					$(this).hide();
  				}
  			});
  			$("#Content .col-md-12#" + blockId).fadeIn("normal");
  		} else {
  			$("#Content .col-md-12").fadeIn("normal");
  		}

  		return false;
  	});
  });
}(jQuery));
;
